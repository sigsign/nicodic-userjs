/**
 * モバイル版掲示板コメントの要素を取得する
 *
 * @param {Element} body 掲示板コメントを含む親要素
 * @returns {Array} 掲示板コメントの要素の配列
 */
const getMobileComments = (body: Element): Element[] => {
    return Array.from(body.querySelectorAll('ul.sw-Article_List > li'))
}

/**
 * モバイル版の要素からレス番を取得する
 *
 * @param {Element} comment 掲示板コメントの要素
 * @returns {number|null} レス番
 */
const getMobileCommentNumber = (comment: Element): number | null => {
    const number = comment.querySelector('.at-List_Name > .at-List_Num')
    if (!number) {
        return null
    }
    const str = number.textContent || ''
    const num = Number(str)
    return str.trim() === num.toString() ? num : null
}

/**
 * モバイル版の要素から投稿者名を取得する
 *
 * @param {Element} comment 掲示板コメントの要素
 * @returns {string|null} 投稿者名
 */
const getMobileCommentName = (comment: Element): string | null => {
    const name = comment.querySelector('.at-List_Name')
    if (!name) {
        return null
    }
    const text = name.lastChild
    return text && text.nodeType === text.TEXT_NODE ? text.textContent : ''
}

/**
 * モバイル版の要素から日付と投稿者IDを取得する
 *
 * @param {Element} comment 掲示板コメントの要素
 * @returns {string|null} 日付と投稿者ID
 */
const getMobileCommentInfo = (comment: Element): string | null => {
    const info = comment.querySelector('.at-List_Date')
    return info ? info.textContent : null
}

/**
 * モバイル版の要素から本文を取得する
 *
 * @param {Element} comment 掲示板コメントの要素
 * @returns {string|null} 本文の HTML
 */
const getMobileCommentBody = (comment: Element): string | null => {
    const body = comment.querySelector('.at-List_Text')
    return body ? body.innerHTML : null
}

const mobileParser = {
    getComments: getMobileComments,
    getCommentNumber: getMobileCommentNumber,
    getCommentName: getMobileCommentName,
    getCommentInfo: getMobileCommentInfo,
    getCommentBody: getMobileCommentBody,
}

export {
    mobileParser,
    getMobileComments,
    getMobileCommentNumber,
    getMobileCommentName,
    getMobileCommentInfo,
    getMobileCommentBody,
}
