type JsonSerializable =
    | string
    | number
    | boolean
    | Array<JsonSerializable>
    | Map<JsonSerializable, JsonSerializable>
    | null
    | undefined

// eslint-disable-next-line @typescript-eslint/no-unused-vars
declare function completion(result?: JsonSerializable): void
