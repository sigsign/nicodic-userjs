import {
    getDesktopComments,
    getDesktopCommentNumber,
    getDesktopCommentName,
    getDesktopCommentInfo,
    getDesktopCommentBody,
} from '@lib/parseDesktopComments'

import {
    getMobileComments,
    getMobileCommentNumber,
    getMobileCommentName,
    getMobileCommentInfo,
    getMobileCommentBody,
} from '@lib/parseMobileComments'

/**
 * 掲示板コメントの要素を取得するラッパー
 *
 * @param {Element} body 掲示板コメントを含む親要素
 * @returns {Array} 掲示板コメントの要素の配列
 */
const getComments = (body: Element): Element[] => {
    const fn = body.querySelector('ul.sw-Article_List')
        ? getMobileComments
        : getDesktopComments
    return fn(body)
}

/**
 * レス番を取得するラッパー
 *
 * @param {Element} comment 掲示板コメントの要素
 * @returns {number|null} レス番
 */
const getCommentNumber = (comment: Element): number | null => {
    const fn =
        comment.tagName.toLowerCase() === 'li'
            ? getMobileCommentNumber
            : getDesktopCommentNumber
    return fn(comment)
}

/**
 * 投稿者名を取得するラッパー
 *
 * @param {Element} comment 掲示板コメントの要素
 * @returns {string|null} 投稿者名
 */
const getCommentName = (comment: Element): string | null => {
    const fn =
        comment.tagName.toLowerCase() === 'li'
            ? getMobileCommentName
            : getDesktopCommentName
    return fn(comment)
}

/**
 * 日付と投稿者IDを取得するラッパー
 *
 * @param {Element} comment 掲示板コメントの要素
 * @returns {string|null} 日付と投稿者ID
 */
const getCommentInfo = (comment: Element): string | null => {
    const fn =
        comment.tagName.toLowerCase() === 'li'
            ? getMobileCommentInfo
            : getDesktopCommentInfo
    return fn(comment)
}

/**
 * 要素から日付を取得する
 *
 * @param {Element} comment 掲示板コメントの要素
 * @returns {Date|null} 日付
 */
const getCommentDate = (comment: Element): Date | null => {
    const info = getCommentInfo(comment)
    if (!info || isDeleted(info)) {
        return null
    }
    const re = /^(\d{4})\/(\d{2})\/(\d{2})\(.\) ([\d:]+) ID:/
    const m = re.exec(info)
    if (!m) {
        return null
    }
    const date = new Date(`${m[1]}-${m[2]}-${m[3]}T${m[4]}.000+09:00`)
    return !Number.isNaN(date) ? date : null
}

/**
 * 要素から投稿者IDを取得する
 *
 * @param {Element} comment 掲示板コメントの要素
 * @returns {string|null} 投稿者ID
 */
const getCommentID = (comment: Element): string | null => {
    const info = getCommentInfo(comment)
    if (!info) {
        return null
    }
    const re = / ID: ([^\s]+)$/
    const match = re.exec(info)
    return match ? match[1] : null
}

/**
 * 本文を取得するラッパー
 *
 * @param {Element} comment 掲示板コメントの要素
 * @returns {string|null} 本文の HTML
 */
const getCommentBody = (comment: Element): string | null => {
    const fn = comment.querySelector('.at-List_Text')
        ? getMobileCommentBody
        : getDesktopCommentBody
    return fn(comment)
}

/**
 * 投稿が削除されているか判定する
 * （削除されている場合は日付が置換されている）
 *
 * @param {string} info 日付と投稿者IDの文字列
 * @returns {boolean} 削除されているかどうか（真偽値）
 */
const isDeleted = (info: string): boolean => {
    const re = /^削除しました ID:/
    return re.test(info)
}

export {
    getComments,
    getCommentNumber,
    getCommentName,
    getCommentInfo,
    getCommentDate,
    getCommentID,
    getCommentBody,
    isDeleted,
}
