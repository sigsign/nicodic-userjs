/**
 * First Contentful Paint (FCP) を DOMContentLoaded 以降に遅延させる
 *
 * 通常時は FCP が DOMContentLoaded より早いため、UserJS の適用時にチラつきが発生する。
 * あえて FCP を遅延させることでチラつきを防ぐ。
 */
const delayFirstContentfulPaint = (): void => {
    if (document.readyState !== 'loading') {
        return
    }
    document.documentElement.style.visibility = 'hidden'
    document.addEventListener('DOMContentLoaded', () => {
        requestAnimationFrame(() => {
            document.documentElement.style.visibility = ''
        })
    })
}

export { delayFirstContentfulPaint }
