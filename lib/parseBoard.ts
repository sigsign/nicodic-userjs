/**
 * 掲示板コンテナを取得する
 *
 * @param {Element} body 掲示板コンテナを含む親要素
 * @returns {HTMLElement|null} 掲示板コンテナ
 */
const getBoard = (body: Element): HTMLElement | null => {
    const selector = body.querySelector('.sw-Article_List')
        ? '.sw-Article_List'
        : '.st-bbs-contents'
    return body.querySelector<HTMLElement>(selector)
}

/**
 * ページャーを取得する
 *
 * @param {Element} body ページャーを含む親要素
 * @returns {Array<HTMLElement>} ページャーの配列
 */
const getPagers = (body: Element): HTMLElement[] => {
    if (!body.querySelector('.sw-Article_List')) {
        return []
    }
    return Array.from(body.querySelectorAll<HTMLElement>('.sw-Paging'))
}

/**
 * ページャーのリンク要素を取得する
 *
 * @param {Element} body リンク要素を含む親要素
 * @returns {Array<HTMLLinkElement>} リンク要素の配列
 */
const getPagerLinks = (body: Element): HTMLLinkElement[] => {
    const selector = body.querySelector('.sw-Article_List')
        ? '.sw-Paging a'
        : '.st-bbs-contents .st-pg_contents a'
    return Array.from(body.querySelectorAll<HTMLLinkElement>(selector))
}

export { getBoard, getPagers, getPagerLinks }
