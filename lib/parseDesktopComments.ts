/**
 * デスクトップ版掲示板コメントの要素を取得する
 *
 * @param {Element} body 掲示板コメントを含む親要素
 * @returns {Array} 掲示板コメントの要素の配列
 */
const getDesktopComments = (body: Element): Element[] => {
    return Array.from(
        body.querySelectorAll('.st-bbs-contents > dl > dt.st-bbs_reshead')
    )
}

/**
 * デスクトップ版の要素からレス番を取得する
 *
 * @param {Element} comment 掲示板コメントの要素
 * @returns {number|null} レス番
 */
const getDesktopCommentNumber = (comment: Element): number | null => {
    const number = comment.querySelector('.st-bbs_resNo')
    if (!number) {
        return null
    }
    const str = number.textContent || ''
    const num = Number(str)
    return str.trim() === num.toString() ? num : null
}

/**
 * デスクトップ版の要素から投稿者名を取得する
 *
 * @param {Element} comment 掲示板コメントの要素
 * @returns {string|null} 投稿者名
 */
const getDesktopCommentName = (comment: Element): string | null => {
    const name = comment.querySelector('.st-bbs_name')
    const trip = comment.querySelector('.st-bbs_resInfo > .trip')
    if (!name) {
        return null
    }
    return !trip ? name.textContent : `${name.textContent}${trip.textContent}`
}

/**
 * デスクトップ版の要素から日付と投稿者IDを取得する
 *
 * @param {Element} comment 掲示板コメントの要素
 * @returns {string|null} 日付と投稿者ID
 */
const getDesktopCommentInfo = (comment: Element): string | null => {
    const info = comment.querySelector('.st-bbs_resInfo')
    if (!info) {
        return null
    }
    const text = info.lastChild
    if (!text || text.nodeType !== text.TEXT_NODE || !text.textContent) {
        return null
    }
    return text.textContent.replace(/\s+/g, ' ').trim()
}

/**
 * デスクトップ版の要素から本文を取得する
 *
 * @param {Element} comment 掲示板コメントの要素
 * @returns {string|null} 本文の HTML
 */
const getDesktopCommentBody = (comment: Element): string | null => {
    const body = comment.nextElementSibling
    return body && body.tagName.toLowerCase() === 'dd' ? body.innerHTML : null
}

const desktopParser = {
    getComments: getDesktopComments,
    getCommentNumber: getDesktopCommentNumber,
    getCommentName: getDesktopCommentName,
    getCommentInfo: getDesktopCommentInfo,
    getCommentBody: getDesktopCommentBody,
}

export {
    desktopParser,
    getDesktopComments,
    getDesktopCommentNumber,
    getDesktopCommentName,
    getDesktopCommentInfo,
    getDesktopCommentBody,
}
