type ListStoreCallback = (value: string[]) => unknown

interface ListStoreChanged {
    readonly key: string
    readonly value: string[]
}

/**
 * タブ区切りテキストを文字列の配列に変換する
 *
 * @param {string} str タブ区切りテキスト
 * @returns {Array} 文字列の配列
 */
const decodeToArray = (str: string): string[] => {
    return str
        .split('\t')
        .filter((str) => {
            return str !== ''
        })
        .map((str) => {
            return str.replace(/\\t/g, '\t')
        })
}

/**
 * 文字列の配列をタブ区切りテキストに変換する
 *
 * @param {Array} arr 文字列の配列
 * @returns {string} タブ区切りテキスト
 */
const encodeFromArray = (arr: string[]): string => {
    return arr
        .map((str) => {
            return str.replace(/\t/g, '\\t')
        })
        .join('\t')
}

/**
 * LocalStorage に配列を保存するクラス
 */
class ListStore {
    readonly key: string

    constructor(readonly prefix: string, private readonly uniqueKey: string) {
        this.key = `${prefix}${uniqueKey}`
    }

    async add(value: string): Promise<void> {
        const array = await this.get()
        if (!array.includes(value)) {
            await this.set([...array, value])
        }
    }

    async contains(value: string): Promise<boolean> {
        const array = await this.get()
        return array.includes(value)
    }

    private createEvent(
        key: string,
        value: string[]
    ): CustomEvent<ListStoreChanged> {
        return new CustomEvent<ListStoreChanged>('liststore:changed', {
            detail: {
                key: key,
                value: value,
            },
        })
    }

    private emit(key: string, value: string[]): void {
        if (typeof document !== 'undefined') {
            const event = this.createEvent(key, value)
            document.dispatchEvent(event)
        }
    }

    async get(): Promise<string[]> {
        try {
            return decodeToArray(localStorage.getItem(this.key) || '')
        } catch (e) {
            throw new Error('localStorage.getItem() is failed')
        }
    }

    on(listener: ListStoreCallback): () => void {
        const storageListener = (ev: StorageEvent) => {
            if (ev.key && ev.key === this.key) {
                listener(decodeToArray(ev.newValue || ''))
            }
        }
        const changedListener = (ev: Event) => {
            const { key, value } = (ev as CustomEvent<ListStoreChanged>).detail
            if (key === this.key) {
                listener(value)
            }
        }
        const disposer = () => {
            window.removeEventListener('storage', storageListener)
            document.removeEventListener('liststore:changed', changedListener)
        }

        window.addEventListener('storage', storageListener, false)
        document.addEventListener('liststore:changed', changedListener, false)
        return disposer
    }

    async remove(value: string): Promise<void> {
        const array = await this.get()
        await this.set(
            array.filter((str) => {
                return str !== value
            })
        )
    }

    async set(array: string[]): Promise<void> {
        try {
            if (array.length === 0) {
                localStorage.removeItem(this.key)
            } else {
                localStorage.setItem(this.key, encodeFromArray(array))
            }
        } catch (e) {
            throw new Error('localStorage.setItem() is failed')
        }
        this.emit(this.key, array)
    }
}

export { ListStore }
