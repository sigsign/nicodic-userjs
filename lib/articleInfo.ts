type ArticleInfo = {
    readonly title: string
    readonly path: string
}

/**
 * ニコニコ大百科の記事タイトルを返す
 * （記事ページ以外は空白を返す）
 *
 * @param {string} pathname ニコニコ大百科のパス名 (e.g. '/a/typescript')
 * @returns {string} 記事タイトル (e.g. 'typescript')
 */
const decodeTitle = (pathname: string): string => {
    const re = /^\/[alcviu]\/(.+)/
    const matches = re.exec(normalizePathname(pathname))
    if (!matches) {
        return ''
    }
    return decodeURIComponent(matches[1])
}

/**
 * ニコニコ大百科の正規化されたパス名を返す
 * （記事ページ以外は空白を返す）
 *
 * @param {string} pathname ニコニコ大百科のパス名 (e.g. '/t/b/a/typescript/1-')
 * @returns {string} 正規化された Path (e.g. '/a/typescript')
 */
const normalizePathname = (pathname: string): string => {
    /**
     * /t と /b を除いたパス名
     * - /a: 単語記事
     * - /l: 生放送記事
     * - /c: コミュニティ記事
     * - /v: 動画記事
     * - /i: 商品記事
     * - /u: ユーザー記事
     */
    const re = /^(\/t)?(\/b)?(\/[alcviu]\/[^/]+)/
    const matches = re.exec(pathname)
    if (!matches) {
        return ''
    }
    return matches[3]
}

/**
 * ニコニコ大百科掲示板のスレッド位置を返す
 *
 * @param {string} pathname ニコニコ大百科のパス名 (e.g. '/t/b/a/typescript/31-')
 * @returns {number} 掲示板のスレッド位置 (e.g. 31)
 */
const parseBoardPosition = (pathname: string): number => {
    const re = /^(\/t)?\/b\/[alviu]\/[^/]+\/(\d+)-$/
    const matches = re.exec(pathname)
    if (!matches) {
        return 0
    }
    return Number(matches[2])
}

/**
 * ニコニコ大百科の URL から記事タイトルと正規化された Path を抽出する
 * （記事以外の場合は title と Path が空白になる）
 *
 * @param {string} url ニコニコ大百科の URL (e.g. 'https://dic.nicovideo.jp/a/typescript')
 * @returns {object} title と path をもつ連想配列
 */
const articleInfo = (url: string): ArticleInfo => {
    const u = new URL(url)
    if (u.origin !== 'https://dic.nicovideo.jp') {
        return { title: '', path: '' }
    }
    return Object.freeze({
        title: decodeTitle(u.pathname),
        path: normalizePathname(u.pathname),
    })
}

export { articleInfo, decodeTitle, normalizePathname, parseBoardPosition }
