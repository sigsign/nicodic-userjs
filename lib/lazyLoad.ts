/**
 * 遅延読み込みを有効化する
 *
 * @param {Element} body 掲示板を含む親要素
 * @param {object|undefined} options IntersectionObserver に渡すオプション
 */
const lazyLoadContents = (
    body: Element,
    options?: IntersectionObserverInit
): void => {
    const loadContents = (content: HTMLElement) => {
        if (content.dataset.src) {
            content.setAttribute('src', content.dataset.src)
        }
    }
    const observer = new IntersectionObserver((entries) => {
        for (const entry of entries) {
            if (entry.isIntersecting) {
                loadContents(entry.target as HTMLElement)
            }
        }
    }, options)
    const contents = body.querySelectorAll<HTMLElement>('.lazy-contents')
    for (const content of contents) {
        if ('loading' in content) {
            ;(content as HTMLImageElement).loading = 'lazy'
            loadContents(content)
        } else {
            observer.observe(content)
        }
    }
}

/**
 * 遅延読み込みを有効化する
 *
 * @param {Element} body 掲示板を含む親要素
 * @param {object|undefined} options IntersectionObserver に渡すオプション
 */
const lazyLoadIframes = (
    body: Element,
    options?: IntersectionObserverInit
): void => {
    const loadIframe = (iframe: HTMLIFrameElement) => {
        if (iframe.contentWindow && iframe.dataset.src) {
            iframe.contentWindow.location.replace(iframe.dataset.src)
        }
    }
    const observer = new IntersectionObserver((entries) => {
        for (const entry of entries) {
            if (entry.isIntersecting) {
                loadIframe(entry.target as HTMLIFrameElement)
            }
        }
    }, options)
    const iframes = body.querySelectorAll<HTMLIFrameElement>(
        '.lazy-contents-iframe'
    )
    for (const iframe of iframes) {
        observer.observe(iframe)
    }
}

export { lazyLoadContents, lazyLoadIframes }
