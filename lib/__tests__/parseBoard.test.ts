import axios from 'axios'
import jsdom from 'jsdom'
import * as parser from '@lib/parseBoard'

const { JSDOM } = jsdom

// https://dic.nicovideo.jp/b/a/ニコニコ大百科/1-
const desktopUrl =
    'https://dic.nicovideo.jp/b/a/%E3%83%8B%E3%82%B3%E3%83%8B%E3%82%B3%E5%A4%A7%E7%99%BE%E7%A7%91/1-'
const desktop = axios.get(desktopUrl).then((resp) => {
    const { document } = new JSDOM(resp.data).window
    return document.body
})
// https://dic.nicovideo.jp/t/b/a/ニコニコ大百科/1-
const mobileUrl =
    'https://dic.nicovideo.jp/t/b/a/%E3%83%8B%E3%82%B3%E3%83%8B%E3%82%B3%E5%A4%A7%E7%99%BE%E7%A7%91/1-'
const mobile = axios.get(mobileUrl).then((resp) => {
    const { document } = new JSDOM(resp.data).window
    return document.body
})

test('Get desktop board', () => {
    return desktop.then((body) => {
        const board = parser.getBoard(body)
        expect(board).not.toBeNull()
    })
})

test('Get mobile board', () => {
    return mobile.then((body) => {
        const board = parser.getBoard(body)
        expect(board).not.toBeNull()
    })
})

test('Get desktop pagers', () => {
    return desktop.then((body) => {
        const pagers = parser.getPagers(body)
        expect(pagers.length).toEqual(0)
    })
})

test('Get mobile pagers', () => {
    return mobile.then((body) => {
        const pagers = parser.getPagers(body)
        expect(pagers.length).toEqual(2)
    })
})

test('Get desktop pager links', () => {
    return desktop.then((body) => {
        const links = parser.getPagerLinks(body)
        expect(links.length).toEqual(16)
    })
})

test('Get mobile pager links', () => {
    return mobile.then((body) => {
        const links = parser.getPagerLinks(body)
        expect(links.length).toEqual(4)
    })
})
