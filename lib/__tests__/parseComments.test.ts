import axios from 'axios'
import jsdom from 'jsdom'
import * as parser from '@lib/parseComments'

const { JSDOM } = jsdom

// https://dic.nicovideo.jp/b/a/ニコニコ大百科/1-
const desktopUrl =
    'https://dic.nicovideo.jp/b/a/%E3%83%8B%E3%82%B3%E3%83%8B%E3%82%B3%E5%A4%A7%E7%99%BE%E7%A7%91/1-'
const desktop = axios.get(desktopUrl).then((resp) => {
    const { document } = new JSDOM(resp.data).window
    return parser.getComments(document.body)
})
// https://dic.nicovideo.jp/t/b/a/ニコニコ大百科/1-
const mobileUrl =
    'https://dic.nicovideo.jp/t/b/a/%E3%83%8B%E3%82%B3%E3%83%8B%E3%82%B3%E5%A4%A7%E7%99%BE%E7%A7%91/1-'
const mobile = axios.get(mobileUrl).then((resp) => {
    const { document } = new JSDOM(resp.data).window
    return parser.getComments(document.body)
})

test('Get desktop comment list', () => {
    return desktop.then((headers) => {
        expect(headers.length).toBe(30)
    })
})

test('Get mobile comment list', () => {
    return mobile.then((containers) => {
        expect(containers.length).toBe(30)
    })
})

test('Compare number list', () => {
    const fn = (arr: (Element | null)[]): (number | null)[] => {
        return arr.map((elm) => {
            return elm ? parser.getCommentNumber(elm) : null
        })
    }
    return Promise.all([desktop.then(fn), mobile.then(fn)]).then(
        ([desktop, mobile]) => {
            expect(desktop).toEqual(mobile)
        }
    )
})

test('Compare name list', () => {
    const fn = (arr: (Element | null)[]): (string | null)[] => {
        return arr.map((elm) => {
            return elm ? parser.getCommentName(elm) : null
        })
    }
    return Promise.all([desktop.then(fn), mobile.then(fn)]).then(
        ([desktop, mobile]) => {
            expect(desktop).toEqual(mobile)
        }
    )
})

test('Compare info field list', () => {
    const fn = (arr: (Element | null)[]): (string | null)[] => {
        return arr.map((elm) => {
            return elm ? parser.getCommentInfo(elm) : null
        })
    }
    return Promise.all([desktop.then(fn), mobile.then(fn)]).then(
        ([desktop, mobile]) => {
            expect(desktop).toEqual(mobile)
        }
    )
})

test('Compare date list', () => {
    const fn = (arr: (Element | null)[]): (Date | null)[] => {
        return arr.map((elm) => {
            return elm ? parser.getCommentDate(elm) : null
        })
    }
    return Promise.all([desktop.then(fn), mobile.then(fn)]).then(
        ([desktop, mobile]) => {
            expect(desktop).toEqual(mobile)
        }
    )
})

test('Compare ID list', () => {
    const fn = (arr: (Element | null)[]): (string | null)[] => {
        return arr.map((elm) => {
            return elm ? parser.getCommentID(elm) : null
        })
    }
    return Promise.all([desktop.then(fn), mobile.then(fn)]).then(
        ([desktop, mobile]) => {
            expect(desktop).toEqual(mobile)
        }
    )
})
