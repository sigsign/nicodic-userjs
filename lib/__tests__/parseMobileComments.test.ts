import axios from 'axios'
import jsdom from 'jsdom'
import { mobileParser } from '@lib/parseMobileComments'

const { JSDOM } = jsdom

// https://dic.nicovideo.jp/t/b/a/ニコニコ大百科/1-
const url =
    'https://dic.nicovideo.jp/t/b/a/%E3%83%8B%E3%82%B3%E3%83%8B%E3%82%B3%E5%A4%A7%E7%99%BE%E7%A7%91/1-'
const comments = axios.get(url).then((resp) => {
    const { document } = new JSDOM(resp.data).window
    return mobileParser.getComments(document.body)
})

test('Get comment list', () => {
    return comments.then((containers) => {
        expect(containers.length).toBe(30)
    })
})

test('Parse number', () => {
    return comments.then((containers) => {
        expect(mobileParser.getCommentNumber(containers[0])).toBe(1)
    })
})

test('Parse numbers', () => {
    return comments.then((containers) => {
        const numbers = containers.map((container) => {
            return mobileParser.getCommentNumber(container)
        })
        const expected = [...Array(30)].map((_, i) => i + 1)
        expect(numbers).toEqual(expected)
    })
})

test('Parse name', () => {
    return comments.then((containers) => {
        const expected = 'ななしのよっしん'
        expect(mobileParser.getCommentName(containers[0])).toBe(expected)
    })
})

test('Parse name when deleted', () => {
    return comments.then((containers) => {
        const expected = '削除しました'
        expect(mobileParser.getCommentName(containers[1])).toBe(expected)
    })
})

test('Parse info field', () => {
    return comments.then((containers) => {
        const expected = '2008/05/13(火) 19:29:18 ID: T54fx4LFae'
        expect(mobileParser.getCommentInfo(containers[0])).toBe(expected)
    })
})

test('Parse info field when deleted', () => {
    return comments.then((containers) => {
        const expected = '削除しました ID: JQ4VNB5+5w'
        expect(mobileParser.getCommentInfo(containers[1])).toBe(expected)
    })
})

test('Parse body', () => {
    return comments.then((containers) => {
        const body = mobileParser.getCommentBody(containers[0])?.trim()
        const expected =
            '<a class="auto" href="/t/a/%E3%82%8F%E3%81%81%E3%81%84">わぁい</a>'
        expect(body).toBe(expected)
    })
})

test('Parse body when deleted', () => {
    return comments.then((containers) => {
        const body = mobileParser.getCommentBody(containers[1])?.trim()
        const expected = '削除しました'
        expect(body).toBe(expected)
    })
})
