import { articleInfo } from '@lib/articleInfo'

test('Parse nicodic URL', () => {
    const url = 'https://dic.nicovideo.jp/a/typescript'
    expect(articleInfo(url)).toEqual({
        title: 'typescript',
        path: '/a/typescript',
    })
})

test('Parse nicodic encoded URL', () => {
    const url =
        'https://dic.nicovideo.jp/a/%E3%83%8B%E3%82%B3%E3%83%8B%E3%82%B3%E5%A4%A7%E7%99%BE%E7%A7%91'
    expect(articleInfo(url)).toEqual({
        title: 'ニコニコ大百科',
        path:
            '/a/%E3%83%8B%E3%82%B3%E3%83%8B%E3%82%B3%E5%A4%A7%E7%99%BE%E7%A7%91',
    })
})

test('Normalize nicodic board URL', () => {
    const url = 'https://dic.nicovideo.jp/b/a/typescript/1-'
    expect(articleInfo(url)).toEqual({
        title: 'typescript',
        path: '/a/typescript',
    })
})

test('Normalize nicodic mobile URL', () => {
    const url = 'https://dic.nicovideo.jp/t/a/typescript'
    expect(articleInfo(url)).toEqual({
        title: 'typescript',
        path: '/a/typescript',
    })
})

test('Normalize nicodic mobile borad URL', () => {
    const url = 'https://dic.nicovideo.jp/t/b/a/typescript/1-'
    expect(articleInfo(url)).toEqual({
        title: 'typescript',
        path: '/a/typescript',
    })
})

test('Error when not nicodic URL', () => {
    const url = 'https://www.google.com/'
    expect(articleInfo(url)).toEqual({
        title: '',
        path: '',
    })
})
