import { ListStore } from '@lib/listStore'

const prefix = 'unique_prefix'
const key = 'unique_key'
const arr = ['foo', 'bar', 'baz', 'foo\tbar\tbaz']

beforeEach(() => {
    localStorage.clear()
})

test('Able to access variable', () => {
    const list = new ListStore(prefix, key)
    expect(list.prefix).toEqual(prefix)
    expect(list.key).toEqual(`${prefix}${key}`)
})

test('Return empty list', () => {
    const list = new ListStore(prefix, key)
    return list.get().then((array) => {
        expect(array).toEqual([])
    })
})

test('Set list', () => {
    const list = new ListStore(prefix, key)
    return list.set(arr).then(() => {
        return list.get().then((array) => {
            expect(array).toEqual(arr)
        })
    })
})

test('Set empty list', () => {
    const list = new ListStore(prefix, key)
    return list.set(arr).then(() => {
        return list.get().then((array) => {
            expect(array).toEqual(arr)
        })
    })
})

test('Add value to list', () => {
    const list = new ListStore(prefix, key)
    return list.add('foo').then(() => {
        return list.add('bar').then(() => {
            return list.add('baz').then(() => {
                return list.add('foo\tbar\tbaz').then(() => {
                    return list.get().then((array) => {
                        expect(array).toEqual(arr)
                    })
                })
            })
        })
    })
})

test('Has value in list', () => {
    const list = new ListStore(prefix, key)
    return list.set(arr).then(() => {
        return list.contains('foo\tbar\tbaz').then((exists) => {
            expect(exists).toBeTruthy()
        })
    })
})

test('Has not value in list', () => {
    const list = new ListStore(prefix, key)
    return list.set(arr).then(() => {
        return list.contains('bad value').then((exists) => {
            expect(exists).toBeFalsy()
        })
    })
})

test('Remove value from list', () => {
    const list = new ListStore(prefix, key)
    return list.set(arr).then(() => {
        return list.remove('foo').then(() => {
            return list.contains('foo').then((exists) => {
                expect(exists).toBeFalsy()
            })
        })
    })
})

test('Remove values from list', () => {
    const list = new ListStore(prefix, key)
    return list.set(arr).then(() => {
        return list.remove('foo').then(() => {
            return list.remove('bar').then(() => {
                return list.remove('baz').then(() => {
                    return list.remove('foo\tbar\tbaz').then(() => {
                        return list.get().then((array) => {
                            expect(array).toEqual([])
                        })
                    })
                })
            })
        })
    })
})
