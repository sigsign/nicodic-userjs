import { addDesktopReloadButton, addMobileReloadButton } from '@lib/addButton'
import { lazyLoadContents, lazyLoadIframes } from '@lib/lazyLoad'
import { getBoard, getPagers, getPagerLinks } from '@lib/parseBoard'

type AjaxCallback = (elm: Element) => void

/**
 * 更新ボタンを追加して、更新ボタンにイベントを設定する
 *
 * @param {Element} body 更新ボタンを追加する親要素
 * @param {function|undefined} fn 掲示板に適用するコールバック関数
 */
const addReloadButton = (body: Element, fn?: AjaxCallback) => {
    const button = body.querySelector('.sw-Article_List')
        ? addMobileReloadButton(body)
        : addDesktopReloadButton(body)

    if (!button) {
        return
    }

    button.textContent = '🔄更新'
    button.style.textAlign = 'center'
    button.addEventListener('click', (ev) => {
        ev.preventDefault()
        const board = getBoard(document.body)
        if (board) {
            board.style.filter = 'opacity(0.5)'
            board.style.transition = 'filter 0.1s ease-in'
        }
        const offset = window.scrollY
        replaceBoard(location.href, true, 200, fn)
            .then(() => {
                document.addEventListener(
                    'pjax:complete',
                    () => {
                        window.scrollTo(0, offset)
                    },
                    { once: true }
                )
            })
            .catch((err) => {
                console.error(err)
                location.assign(location.href)
            })
    })
}

/**
 * 掲示板を Ajax 対応にする
 *
 * @param {function|undefined} fn 掲示板に適用するコールバック関数
 */
const ajaxBoard = (fn?: AjaxCallback): void => {
    if (location.origin !== 'https://dic.nicovideo.jp') {
        return
    }
    if (
        !location.pathname.startsWith('/t/b/') &&
        !location.pathname.startsWith('/b/')
    ) {
        return
    }

    setupPagerLinks(document.body, fn)
    addReloadButton(document.body, fn)
    window.addEventListener('popstate', () => {
        const board = getBoard(document.body)
        if (board) {
            board.style.filter = 'opacity(0.5)'
            board.style.transition = 'filter 0.1s ease-in'
        }
        replaceBoard(location.href, false, 200, fn).catch((err) => {
            console.error(err)
            location.assign(location.href)
        })
    })
}

/**
 * データを取得して Document に変換して返す
 *
 * @param {RequestInfo} input 取得したい URL、もしくは Request
 * @param {RequestInit} init fetch のオプション
 * @returns {Promise<Document>} 取得したデータの Document
 */
const fetchDocument = async (input: RequestInfo, init?: RequestInit) => {
    const response = await fetch(input, init)
    if (!response.ok) {
        throw new Error(`Request failed: ${response.status} - ${input}`)
    }
    const parser = new DOMParser()
    return parser.parseFromString(await response.text(), 'text/html')
}

/**
 * 掲示板を Ajax で遷移させる
 *
 * @param {string} url 遷移する URL
 * @param {boolean} fresh キャッシュではなく最新版を取得するかどうか
 * @param {number} minTime 遷移するまでの最短時間 (msec)
 * @param {function|undefined} fn 掲示板に適用するコールバック関数
 */
const replaceBoard = async (
    url: string,
    fresh: boolean,
    minTime: number,
    fn?: AjaxCallback
) => {
    const startAt = Date.now()
    const option: RequestInit = fresh
        ? { cache: 'no-cache' }
        : { cache: 'default' }

    const doc = await fetchDocument(url, option)
    setupPagerLinks(doc.body, fn)
    addReloadButton(doc.body, fn)
    lazyLoadContents(doc.body)
    lazyLoadIframes(doc.body)

    if (fn) {
        fn(doc.body)
    }

    const oldBoard = getBoard(document.body)
    const newBoard = getBoard(doc.body)
    const oldPagers = getPagers(document.body)
    const newPagers = getPagers(doc.body)

    const time = Date.now() - startAt
    setTimeout(
        () => {
            replaceElement(oldBoard, newBoard)
            oldPagers.forEach((pager, i) => {
                replaceElement(pager, newPagers[i])
            })
            document.dispatchEvent(new Event('pjax:complete'))
        },
        minTime > time ? minTime - time : 0
    )
}

/**
 * 要素を置換する
 *
 * @param {HTMLElement|null} oldElement 置換される要素
 * @param {HTMLElement|null} newElement 置換したい要素
 */
const replaceElement = (
    oldElement: HTMLElement | null | undefined,
    newElement: HTMLElement | null | undefined
) => {
    if (oldElement) {
        const parent = oldElement.parentElement
        if (parent && newElement) {
            parent.replaceChild(newElement, oldElement)
        }
    }
}

/**
 * 掲示板の最上部までスクロールする
 *
 * @param {Element} body 掲示板を含む親要素
 * @param {string} pathname パス名
 */
const scrollTop = (body: Element, pathname: string) => {
    if (pathname.startsWith('/t/')) {
        const pagers = getPagers(body)
        if (pagers[0]) {
            pagers[0].scrollIntoView(true)
        }
    } else {
        const board = getBoard(body)
        if (board) {
            board.scrollIntoView(true)
        }
    }
}

/**
 * ページャーにイベントを設定する
 *
 * @param {Element} body リンク要素を含む親要素
 * @param {function|undefined} fn 掲示板に適用するコールバック関数
 */
const setupPagerLinks = (body: Element, fn?: AjaxCallback) => {
    for (const link of getPagerLinks(body)) {
        link.addEventListener('click', (ev) => {
            ev.preventDefault()
            const board = getBoard(document.body)
            if (board) {
                board.style.filter = 'opacity(0.5)'
                board.style.transition = 'filter 0.1s ease-in 0.2s'
            }
            const url = (ev.target as HTMLLinkElement).href
            replaceBoard(url, false, 0, fn)
                .then(() => {
                    document.addEventListener(
                        'pjax:complete',
                        () => {
                            scrollTop(document.body, location.pathname)
                        },
                        { once: true }
                    )
                    history.pushState({}, '', url)
                })
                .catch((err) => {
                    console.error(err)
                    location.assign(url)
                })
        })
    }
}

export { ajaxBoard }
