import { ListStore } from '@lib/listStore'
import { getCommentID } from '@lib/parseComments'

interface NGButton extends HTMLSpanElement {
    dispose?: () => void
}

type ReloadButton = HTMLDivElement | HTMLLIElement

const enum ClassName {
    button = 'us-NGButton',
    hidden = 'us-NGID',
    reload = 'us-Reload',
    view = 'us-NGView',
}

const enum Selector {
    button = '.us-NGButton',
}

/**
 * 掲示板に NG ボタンを追加する
 *
 * @param {Element} elm NG ボタンを追加する掲示板コメントの要素
 * @param {ListStore} list NGID を保存している ListStore
 * @returns {NGButton} 追加したボタン要素
 */
const addButton = (elm: Element, list: ListStore): NGButton => {
    const exists = elm.querySelector<NGButton>(Selector.button)
    if (exists) {
        return exists
    }

    const button: NGButton = document.createElement('span')
    button.classList.add(ClassName.button)

    const id = getCommentID(elm)
    if (!id) {
        return button
    }

    button.addEventListener('click', () => {
        list.contains(id).then((exists) => {
            exists ? list.remove(id) : list.add(id)
        })
    })
    button.dispose = list.on((value) => {
        value.includes(id)
            ? elm.classList.add(ClassName.hidden)
            : elm.classList.remove(ClassName.hidden, ClassName.view)
    })
    list.contains(id).then((exists) => {
        if (exists) {
            requestAnimationFrame(() => {
                elm.classList.add(ClassName.hidden)
            })
        }
    })
    setupTemporaryDisplay(elm)
    requestAnimationFrame(() => {
        elm.tagName.toLowerCase() === 'li'
            ? elm.insertBefore(button, elm.querySelector('.at-List_Text'))
            : elm.appendChild(button)
    })

    return button
}

/**
 * デスクトップ版に更新ボタンを追加する
 *
 * @param {Element} body 更新ボタンを追加する親要素
 * @returns {ReloadButton|null} 追加した更新ボタン
 */
const addDesktopReloadButton = (body: Element): ReloadButton | null => {
    const link = body.querySelector(
        '.st-bbs-contents .st-pg_contents :last-child'
    )
    if (!link || !link.classList.contains('current')) {
        return null
    }
    const pager = body.querySelector('.st-bbs-contents > div:nth-last-child(3)')
    if (!pager) {
        return null
    }
    const parent = pager.parentElement
    if (!parent) {
        return null
    }

    const button = document.createElement('div')
    button.classList.add(ClassName.reload)
    parent.insertBefore(button, pager)

    return button
}

/**
 * モバイル版に更新ボタンを追加する
 *
 * @param {Element} body 更新ボタンを追加する親要素
 * @returns {ReloadButton|null} 追加した更新ボタン
 */
const addMobileReloadButton = (body: Element): ReloadButton | null => {
    if (body.querySelector('.sw-Paging .sw-Paging_Next-nextbtn a')) {
        return null
    }
    const board = body.querySelector('ul.sw-Article_List')
    if (!board) {
        return null
    }

    const button = document.createElement('li')
    button.classList.add(ClassName.reload)
    board.appendChild(button)

    return button
}

/**
 * NG されていても名前欄をクリックしたら一時的にコメントを表示する
 *
 * @param {Element} elm 掲示板コメントの要素
 */
const setupTemporaryDisplay = (elm: Element): void => {
    const selector =
        elm.tagName.toLowerCase() === 'li' ? '.at-List_Name' : '.st-bbs_name'
    const name = elm.querySelector(selector)
    if (!name) {
        return
    }
    name.addEventListener('click', () => {
        if (!elm.classList.contains(ClassName.hidden)) {
            return
        }
        elm.classList.contains(ClassName.view)
            ? elm.classList.remove(ClassName.view)
            : elm.classList.add(ClassName.view)
    })
}

export {
    NGButton,
    ReloadButton,
    addButton,
    addDesktopReloadButton,
    addMobileReloadButton,
}
