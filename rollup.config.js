import typescript from '@rollup/plugin-typescript';
import postcss from 'rollup-plugin-postcss'
import metablock from 'rollup-plugin-userscript-metablock';

import cssnano from 'cssnano'

const asyncInjectCSS = (css) => {
    return `
const asyncInjectCSS = (css) => {
    const style = document.createElement('style')
    style.setAttribute('type', 'text/css')
    style.appendChild(document.createTextNode(css))
    if (document.readyState !== 'loading') {
        document.head.appendChild(style)
    } else {
        document.addEventListener('DOMContentLoaded', () => {
            document.head.appendChild(style)
        })
    }
}
asyncInjectCSS(${css})`
}

export default [{
    input: 'src/banComments.ts',
    output: {
        file: 'dist/banComments.user.js',
        format: 'iife',
        indent: false,
    },
    plugins: [
        postcss({
            plugins: [
                cssnano,
            ],
            inject: asyncInjectCSS,
        }),
        typescript(),
        metablock({
            file: 'meta/banComments.json'
        }),
    ],
}]
