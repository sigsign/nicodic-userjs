ニコニコ大百科掲示板に NGID ボタンを追加します。

Firefox や Chrome のほか、iOS Safari のショートカット機能でも利用できるようになっています。

## 機能

- ID の横に NGID ボタンを追加します
- 掲示板のページ移動を Ajax 対応にします
  - iOS ショートカットは GreaseMonkey などと違って自動実行されないため、手動実行する回数を減らすための苦肉の策です
  - 思った以上に便利だったので PC 版でも動くようにしました
- 掲示板の最終ページに更新ボタンを追加します
  - 最新レスを表示するために再読込する必要がなくなります

## 対応環境

- 最新版の Chrome / Edge (TamperMonkey, ViolentMonkey)
- 最新版の Firefox および Firefox ESR (GreaseMonkey, TamperMonkey, ViolentMonkey)
- iOS Safari 14
  - Safari 以外のブラウザ（Chrome for iOSなど）はショートカットに対応していないため実行できません

## iOS 向けインストールガイド

### インストール方法

1. ショートカットアプリを開きます
1. 右上の [+] ボタンをタップして新規ショートカットを作成します
1. 右上の […] ボタンをタップして詳細設定を開きます
1. 「共有シートに表示」を有効化します
1. 「共有シートタイプ」で「SafariのWebページ」以外のチェックを外します
1. 右上の [完了] ボタンをタップして詳細設定を閉じます
1. 中央の [+] ボタンをタップして「Web」から「WebページでJavaScriptを実行」アクションを追加します
1. デフォルトで入力されている JavaScript を消します
1. この UserScript をコードタブからコピーして入力画面に貼り付けます（GreasyFork より [GitLab](https://gitlab.com/sigsign/nicodic-userjs/-/blob/master/dist/banComments.user.js) のほうがコピーが簡単にできます。中身は同じです。「Web IDE」の右隣の「クリップボードにコピー」ボタンをタップしてください）
1. [次へ] ボタンをタップして、ショートカットに名前を付けて保存します

これでインストールは完了です。

Apple 公式のヘルプも参考にしてください。

- [「ショートカット」の「WebページでJavaScriptを実行」アクションの概要 - Apple サポート](https://support.apple.com/ja-jp/guide/shortcuts/apd218e2187d/4.0/ios/14.0)
- [「ショートカット」の「WebページでJavaScriptを実行」アクションを使う - Apple サポート](https://support.apple.com/ja-jp/guide/shortcuts/apdb71a01d93/ios)

### 実行方法

Safari でニコニコ大百科掲示板を開き、共有ボタンからショートカットを実行してください。

ショートカットは自動的に実行されないため、掲示板を開くたびに実行する必要があります。

初回のみ「nicovideo.jpへのアクセスを許可しますか？」と確認画面が表示されます。スクリプトの中身を確認した上で許可してください。

### 注意事項

- **ショートカットは iOS Safari 以外のブラウザでは利用できません**
