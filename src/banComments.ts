import { NGButton, addButton } from '@lib/addButton'
import { normalizePathname } from '@lib/articleInfo'
import { ajaxBoard } from '@lib/ajaxBoard'
import { delayFirstContentfulPaint } from '@lib/delayFirstContentfulPaint'
import { ListStore } from '@lib/listStore'
import { getComments } from '@lib/parseComments'

import '../styles/banComments.css'

type ThreadList = {
    readonly url: string
    readonly container: Element
}

/**
 * 実行タイミングに関わらず関数を実行する
 *
 * @param {function} fn 実行したい関数
 */
const exactRun = (fn: () => void) => {
    if (document.readyState !== 'loading') {
        fn()
    }
    document.addEventListener('DOMContentLoaded', fn)
}

/**
 * 「最近書かれた掲示板のレス」ページで URL とコンテナ要素を抽出する
 *
 * @returns {Array<object>} { url: string, container: Element } の配列
 */
const getLatestPosts = (): ThreadList[] => {
    const selector = '.st-pg_link-returnArticle a'
    return Array.from(document.querySelectorAll('.st-bbs-contents'))
        .filter((elm) => {
            return elm.querySelector(selector)
        })
        .map((elm) => {
            const link = elm.querySelector(selector) as HTMLAnchorElement
            return { url: link.href, container: elm }
        })
}

/**
 * 初期設定を行う
 *
 * @param {string} url 記事の URL
 * @param {Element} elm 掲示板コメントを含む親要素
 */
const init = (url: string, elm: Element) => {
    const u = new URL(url)
    if (u.origin !== 'https://dic.nicovideo.jp') {
        return
    }
    const key = normalizePathname(u.pathname)
    const ngList = new ListStore('__BC__', key)
    getComments(elm).forEach((comment) => {
        addButton(comment, ngList)
    })
    return ngList
}

if (
    location.pathname.startsWith('/b/') ||
    location.pathname.startsWith('/t/b/') ||
    location.pathname.startsWith('/m/n/res/')
) {
    delayFirstContentfulPaint()
}

if (!location.pathname.startsWith('/m/n/res/')) {
    exactRun(() => {
        const list = init(location.href, document.documentElement)
        if (list) {
            ajaxBoard((elm) => {
                const buttons = document.querySelectorAll<NGButton>(
                    '.us-NGButton'
                )
                buttons.forEach((button) => {
                    if (button.dispose) {
                        button.dispose()
                    }
                })
                getComments(elm).forEach((comment) => {
                    addButton(comment, list)
                })
            })
        }
    })
} else {
    exactRun(() => {
        getLatestPosts().forEach((thread) => {
            init(thread.url, thread.container)
        })
    })
}

// iOS Shortcut 対応
if (typeof completion === 'function') {
    completion()
}
