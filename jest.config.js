/* eslint-disable no-undef */
module.exports = {
  roots: [
    '<rootDir>/lib',
  ],
  testMatch: [
    '**/__tests__/**/*.+(ts|js)',
  ],
  moduleNameMapper: {
    '^@lib/(.*)$': '<rootDir>/lib/$1',
  },
  resetMocks: false,
  setupFiles: [
    "jest-localstorage-mock"
  ],
  preset: 'ts-jest',
  testEnvironment: 'node',
};
